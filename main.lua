require "lib.slam"
vector = require "lib.hump.vector"
Timer = require "lib.hump.timer"
Camera = require "lib.hump.camera"
tlfres = require "lib.tlfres"

require "helpers"

CANVAS_WIDTH = 1600
CANVAS_HEIGHT = 900

function love.load()
    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename)
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            for fontsize=50,100 do
                fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
            end
        end
    end

    -- set up physics
    love.physics.setMeter(100)
    world = love.physics.newWorld(0, 0, true)
    world:setCallbacks(beginContact)

    -- set up camera
    camera = Camera(0, 0)
    camera:zoom(1)

    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)

    -- set up game objects
    player = {x=0, y=0, vx=0, vy=0}
end

function love.update(dt)
    Timer.update(dt)
    world:update(dt)

    -- react to inputs
    if love.keyboard.isDown("left") then
        player.vx = player.vx - dt*50
    end
    if love.keyboard.isDown("right") then
        player.vx = player.vx + dt*50
    end
    if love.keyboard.isDown("up") then
        player.vy = player.vy - dt*50
    end
    if love.keyboard.isDown("down") then
        player.vy = player.vy + dt*50
    end

    -- update game objects
    player.x = player.x + player.vx
    player.y = player.y + player.vy

    player.vx = player.vx*0.9
    player.vy = player.vy*0.9
end

function love.keypressed(key)
    if key == "escape" then
        love.window.setFullscreen(false)
        love.event.quit()
    end
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function love.mousepressed(x, y, button, touch)
    x, y = tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)

    -- calculate world coordinates
    wx, wy = camera:worldCoords(x, y, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)

    if button == 1 then
        player.x = wx
        player.y = wy
    elseif button == 2 then

    end
end

function love.draw()
    love.graphics.setColor(255, 255, 255, 255)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    camera:attach(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, true)

    -- draw world
    love.graphics.draw(images.love_game_icon, player.x, player.y, 0, 1, 1, images.love_game_icon:getWidth()/2, images.love_game_icon:getHeight()/2)

    camera:detach()

    -- draw UI

    tlfres.endRendering()
end

function beginContact(a, b, coll)

end
